Clear-Host

if ((Get-ExecutionPolicy -Scope CurrentUser) -ne "Unrestricted") {
  Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope CurrentUser -Force | Out-Null
}

Set-Location -Path $env:temp

if (Test-Path .\sigma.ps1) {
  Remove-Item -Path .\sigma.ps1
}

(New-Object System.Net.WebClient).DownloadString("https://gitlab-y7r4iuqy1m.sigmasms.ru/sigma/environment/init-scripts/-/raw/main/sigma.ps1") | Out-File -FilePath $env:temp\sigma.ps1

.\sigma.ps1