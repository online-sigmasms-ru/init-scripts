Clear-Host

if ((Get-ExecutionPolicy -Scope CurrentUser) -ne "Unrestricted") {
  Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope CurrentUser -Force | Out-Null
}

if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
  Start-Process -FilePath "powershell" -ArgumentList "-NoProfile -ExecutionPolicy Unrestricted -File $PSCommandPath" -Verb RunAs -WindowStyle Maximized -Wait
  Exit
}

Set-Location -Path $env:temp

if (!(Test-Path "C:\ProgramData\chocolatey\bin\choco.exe")) {
  if (!(Test-Path .\chocoInstall.ps1)) {
    Write-Host -NoNewline "Download chocolatey installation script..."
    Invoke-WebRequest -Uri "https://chocolatey.org/install.ps1" -OutFile .\chocoInstall.ps1 | Out-Null
    Write-Host -ForegroundColor Green "done"
  }

  Write-Host -NoNewline "Install chocolatey..."
  .\chocoInstall.ps1 | Out-Null
  Write-Host -ForegroundColor Green "done"
}

$dockerDesktop = "C:\Program Files\Docker\Docker\Docker Desktop.exe"
if (!(Test-Path $dockerDesktop)) {
  Write-Host -NoNewline "Install docker desktop..."
  choco install docker-desktop --accept-license --confirm --force --no-progress --silent
  Write-Host -ForegroundColor Green "done"
}