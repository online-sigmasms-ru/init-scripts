Clear-Host

$ErrorActionPreference = 'Inquire'

if ((Get-ExecutionPolicy -Scope CurrentUser) -ne "Unrestricted") {
  Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope CurrentUser -Force | Out-Null
}

if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
  Start-Process -FilePath "powershell" -ArgumentList "-NoProfile -ExecutionPolicy Unrestricted -File $PSCommandPath" -Verb RunAs -WindowStyle Maximized -Wait
  Exit
}

if ([Environment]::OSVersion.Version.Build -lt 18362) {
  Write-Error "WSL 2 only support Windows version build 18362. Please update."
}

Set-Location -Path $env:temp

if (Test-Path .\install-docker-desktop.ps1) {
  Remove-Item -Path .\install-docker-desktop.ps1
}

(New-Object System.Net.WebClient).DownloadString("https://gitlab-y7r4iuqy1m.sigmasms.ru/sigma/environment/init-scripts/-/raw/main/install-docker-desktop.ps1") | Out-File -FilePath $env:temp\install-docker-desktop.ps1
Start-Process -FilePath "powershell" -ArgumentList ".\install-docker-desktop.ps1" -Verb RunAs -WindowStyle Normal -Wait

$dockerProcess = Get-Process "*docker desktop*"
if ($dockerProcess.Count -gt 0) {
  Write-Host -NoNewline "Stop docker destop process if it running..."
  $dockerProcess[0].Kill()
  $dockerProcess[0].WaitForExit()
  Write-Host -ForegroundColor Green "done"
}

$windowsOptionalFeatures = @(
  "VirtualMachinePlatform",
  "Microsoft-Windows-Subsystem-Linux",
  
  "Microsoft-Hyper-V-All",
  "Microsoft-Hyper-V-Tools-All",
  "Microsoft-Hyper-V-Management-PowerShell"
)

foreach ($feature in $windowsOptionalFeatures) {
  if ((Get-WindowsOptionalFeature -Online -FeatureName $feature).State -eq "Disabled") {
    Write-Host -NoNewline "Enable Windows optional feature $feature..."
    Start-Process -FilePath "powershell" -ArgumentList "Enable-WindowsOptionalFeature -Online -FeatureName $feature -All -NoRestart" -Verb RunAs -WindowStyle Normal -Wait
    Write-Host -ForegroundColor Green "done"
  }
}

$linuxKernelUpdatedPath = "$env:USERPROFILE\.linuxKernelUpdated"
if (!(Test-Path $linuxKernelUpdatedPath)) {
  Write-Host -NoNewline "Update Linux Kernel..."

  $linuxKernelUpdatePath = "$env:TEMP\wsl_update_x64.msi"
  Start-Process -FilePath "powershell" -ArgumentList "Invoke-WebRequest -Uri https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi -OutFile $linuxKernelUpdatePath" -Verb RunAs -WindowStyle Normal -Wait
  Start-Process -FilePath "msiexec" -ArgumentList "/I $linuxKernelUpdatePath /quiet" -WindowStyle Hidden -Wait
  Start-Process -FilePath "powershell" -ArgumentList "New-Item -ItemType 'file' -Path $linuxKernelUpdatedPath" -Verb RunAs -WindowStyle Hidden -Wait

  Write-Host -ForegroundColor Green "done"
}

Write-Host -NoNewline "Set default version of WSL 2..."
Start-Process -FilePath "powershell" -ArgumentList "wsl --set-default-version 2" -Verb RunAs -WindowStyle Hidden -Wait
Write-Host -ForegroundColor Green "done"

Write-Host -NoNewline "Update WSL..."
wsl --update
Write-Host -ForegroundColor Green "done"

if (!(wsl -l | Where {$_.Replace("`0","") -match '^Ubuntu'})) {
  Write-Host -NoNewline "Install Ubuntu..."
  wsl --install -d Ubuntu
  Write-Host -ForegroundColor Green "done"
}

if (wsl -l | Where {$_.Replace("`0","") -match '^Ubuntu'}) {
  wsl --setdefault Ubuntu
  Write-Host -NoNewline "Run sigma.sh on ubuntu..."
  Start-Process -FilePath "powershell" -ArgumentList "ubuntu.exe run 'cd ~; rm ./sigma.sh; wget https://gitlab-y7r4iuqy1m.sigmasms.ru/sigma/environment/init-scripts/-/raw/main/sigma.sh; chmod u+x ./sigma.sh; ./sigma.sh'" -Verb RunAs -WindowStyle Normal -Wait
  Write-Host -ForegroundColor Green "done"
}

$dockerProcess = Get-Process "*docker desktop*"
$dockerDesktop = "C:\Program Files\Docker\Docker\Docker Desktop.exe"
if ($dockerProcess.Count -eq 0) {
  if (Test-Path $dockerDesktop) {
    Write-Host -NoNewline "Runing docker desktop process..."
    Start-Process $dockerDesktop
    Write-Host -ForegroundColor Green "done"
  }
}

Write-Host ""
Write-Host "All operation done."

Read-Host "Press any key to close this window..."
