#!/bin/bash

set -e

SSH_DIR=$HOME/.ssh
SSH_PUBLIC_KEY_FILE=$SSH_DIR/id_rsa.pub

GITLAB_LINK=gitlab.com
GITLAB_PATH_TO_API=api/v4
GITLAB_LINK_TO_API=$GITLAB_LINK/$GITLAB_PATH_TO_API
GITLAB_LINK_FULL=https://$GITLAB_LINK
GITLAB_LINK_TO_API_FULL=$GITLAB_LINK_FULL/$GITLAB_PATH_TO_API

SIGMASMS_REGISTRY=@sigmasms
SIGMASMS_REGISTRY_PROJECT_ID=59
SIGMASMS_CLI=$SIGMASMS_REGISTRY/cli

NPMRC_FILE=$HOME/.npmrc

clear

sudo apt update
sudo apt upgrade -y

clear

print_text() {
  echo $*
}

git_info() {
  echo "Необходимо заполнить базовую информацию для конфигурации git."
  if [ ! $(git config user.name) >/dev/null ]; then
    read -p "Введите имя пользователя: " username
    git config --global user.name $username
  fi
  if [ ! $(git config user.email) >/dev/null ]; then
    read -p "Введите e-mail пользователя: " email
    git config --global user.email $email
  fi
}

make_ssh_key() {
  mkdir -p $SSH_DIR -m 0700
  ssh-keygen -q -t rsa -N "" -f $SSH_DIR/id_rsa
  echo "Откройте раздел SSH ключей в персональных настройках GitLab и добавьте туда сгенерированный ключ."
  echo "GitLab > Preferences > SSH Keys"
  echo "https://gitlab.com/-/profile/keys"
  echo ""
  cat $SSH_PUBLIC_KEY_FILE
  echo ""
  while true; do
    read -p "Продолжить выполнение скрипта? [Y/n]" yn
    case $yn in
    [Yy]*) break ;;
    [Nn]*) exit ;;
    *) break ;;
    esac
  done
}

npm_config() {
  print_text "[ CONFIG ] npm"
  print_text "Необходимо создать токен для установки $SIGMASMS_CLI из реестра $SIGMASMS_REGISTRY."
  print_text "Токен должен иметь минимальную область применения (scope) - read_api."
  print_text "Для этого перейдите на сайт GitLab > Preferences > Access Tokens:"
  print_text "$GITLAB_LINK_FULL/-/profile/personal_access_tokens"
  echo ""
  npm config set -- $SIGMASMS_REGISTRY:registry=$GITLAB_LINK_TO_API_FULL/projects/$SIGMASMS_REGISTRY_PROJECT_ID/packages/npm/
  read -p "Введите созданный токен: " token
  npm config set -- //$GITLAB_LINK_TO_API/projects/$SIGMASMS_REGISTRY_PROJECT_ID/packages/npm/:_authToken=$token
  npm config set always-auth true
  print_text "[ OK ] npm"
}

if which curl >/dev/null; then
  print_text "[ OK ] curl"
else
  print_text "[ INSTALL ] curl"
  sudo apt install curl -y
fi
if which git >/dev/null; then
  print_text "[ OK ] git"
else
  print_text "[ INSTALL ] git"
  sudo apt install git -y
fi

if [ ! $(git config user.name) >/dev/null ] || [ ! $(git config user.email) >/dev/null ]; then
  git_info
fi

if [ -f $SSH_PUBLIC_KEY_FILE ]; then
  print_text "[ OK ] ssh keys"
else
  print_text "[ INSTALL ] ssh keys"
  make_ssh_key
fi

if grep -s -q $GITLAB_LINK $SSH_DIR/known_hosts; then
  print_text "[ OK ] ssh know hosts"
else
  print_text "[ INSTALL ] ssh know hosts"
  ssh-keyscan $GITLAB_LINK >> $SSH_DIR/known_hosts
fi

if [ -d $HOME/init-scripts ]; then
  print_text "[ OK ] init scripts"
else
  print_text "[ CLONE ] init scripts"
  cd $HOME
  git clone git@gitlab-y7r4iuqy1m.sigmasms.ru:sigma/environment/init-scripts.git
fi

if which nvm >/dev/null; then
  print_text "[ OK ] nvm"
else
  print_text "[ INSTALL ] nvm"
  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
  export NVM_DIR="$HOME/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
  [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
  nvm install --lts
  nvm use --lts
fi

if [ ! -f $NPMRC_FILE ]; then
  npm_config
fi
if ! grep -q "projects/$SIGMASMS_REGISTRY_PROJECT_ID/packages/npm/:_authToken=glpat-" $NPMRC_FILE >/dev/null; then
  npm_config
fi

if [ -x "$(command -v sigma)" ]; then
  print_text "[ OK ] $SIGMASMS_CLI"
else
  print_text "[ INSTALL ] $SIGMASMS_CLI $CURRENT_CLI_VERSION"
  npm i --location global $SIGMASMS_CLI
fi

echo ""

print_text "If it first run of script, restart computer, else use comand \"sigma start\"."
